module.exports = app => {
    const persona = require("../controllers/persona.controller.js");
  
    var router = require("express").Router();
  
    
    router.post("/", persona.create);    
    router.get("/", persona.findAll);
    
    app.use("/api/persona", router);
  };