const db = require("../models");
const Persona = db.persona;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {


  // Create a Tutorial
  const persona = {
    nombre: req.body.nombre,
    apellido_paterno: req.body.apellido_paterno,
    apellido_materno: req.body.apellido_materno
  };
  
  // Save Tutorial in the database
  Persona.create(persona)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Error al registrar persona."
      });
    });
};


  exports.findAll = (req, res) => {

    Persona.findAll()
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Error."
        });
      });
  };