module.exports = (sequelize, Sequelize) => {
    const Prestamo = sequelize.define("prestamo", {
  
      idlibro: {
        type: Sequelize.INTEGER,
        references: {
           model: 'libros', 
           key: 'id', 
        }
      },
      idPersona: {
        type: Sequelize.INTEGER,
        references: {
           model: 'personas', 
           key: 'id', 
        }
      },
      fecha: {
        type: Sequelize.DATE
      },
  
  
     
    },
    {freezeTableName: true});
    return Prestamo;
  };
  