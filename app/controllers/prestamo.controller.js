const db = require("../models");
const Prestamo = db.prestamo;
const Persona = db.persona;
const Libro = db.libro;



        exports.create = (req, res) => {

            var dat= new Date()  
            const prestamo = {
            idPersona: req.body.idPersona,
            idlibro: req.body.idlibro,
            fecha: dat
           };
          
            // Save Tutorial in the database
            Prestamo.create(prestamo)
              .then(data => {
                res.status(201).send(data);
              })
              .catch(err => {
                res.status(500).send({
                  message:
                    err.message || "Error al registra Prestamo."
                });
              });
          };
          
          exports.consulta=(req,res)=>
          {
          
            
              try{
                Prestamo.findAll(
                  {
                  include: [{
                    model: Persona,
                    attributes: ['nombre','apellido_paterno','apellido_materno']
              
                  },
                  {
                    model: Libro,
                    attributes: ['titulo']
                  }
                ]
                }
                ).then(data => {
                  res.send(data);
                })
                .catch(err => {
                  res.status(500).send({
                    message:
                      err.message || "Some error occurred while retrieving personas."
                  });
                });;
            
            
              }catch(error)
              {
                res.status(500).send(error)
              }
            
            }          
    
          exports.busqueda=(req,res)=>
            {
            
              
                try{
                  Prestamo.findAll(
                    {
                    include: [{
                      model: Persona,
                      attributes: ['nombre','apellido_paterno','apellido_materno'],
                      where: {id : req.params.idPersona }
                    },
                    {
                      model: Libro,
                      attributes: ['titulo']
                    }
                  ]
                  }
                  ).then(data => {
                    res.send(data);
                  })
                  .catch(err => {
                    res.status(500).send({
                      message:
                        err.message || "Some error occurred while retrieving personas."
                    });
                  });;
              
              
                }catch(error)
                {
                  res.status(500).send(error)
                }
              
             }

          
