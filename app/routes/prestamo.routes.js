module.exports = app => {
    const prestamo = require("../controllers/prestamo.controller.js");
  
    var router = require("express").Router();
  
    router.post("/", prestamo.create);  
    router.get("/",prestamo.consulta);
    router.get("/:idPersona",prestamo.busqueda);

    app.use("/api/prestamo", router);
  };
  