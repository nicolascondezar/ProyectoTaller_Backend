const db = require("../models");
const Prestamo = db.prestamo;
const Persona = db.persona;
const Libro = db.libro;


exports.create = (req, res) => {

  // Create a Tutorial
  const libro = {
    autor: req.body.autor,
    titulo: req.body.titulo,
    ano : req.body.ano
  };

  // Save Tutorial in the database
  Libro.create(libro)
    .then(data => {
      res.status(201).send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Error al registra libro."
      });
    });
};

exports.findAll = (req, res) => {

  Libro.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Error."
      });
    });
};

exports.busquedaL=(req,res)=>
             {
            
               
                 try{
                   Prestamo.findAll(
                     {
                     include: [{
                       model: Libro,
                       attributes: ['titulo','autor','ano'],
                       where: {id : req.params.idlibro }
                     },
                     {
                       model: Persona,
                       attributes: ['nombre','apellido_paterno','apellido_materno']
                     }
                   ]
                   }
                   ).then(data => {
                     res.send(data);
                   })
                   .catch(err => {
                     res.status(500).send({
                       message:
                         err.message || "Some error occurred while retrieving personas."
                     });
                   });;
               
               
                 }catch(error)
                 {
                   res.status(500).send(error)
                 }
               
              }



